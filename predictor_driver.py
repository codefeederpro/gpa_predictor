def predictor(a,b,c,branch):
    
    import statsmodels.api as sm
    import pandas as pd
    
    
    d1 = {'col1': [a], 'col2': [b],'col3': [c]}
    df1 = pd.DataFrame(data=d1)
    #print(df1.head(10))
    g='PREDICTOR_'+branch+'.pickle'
    
    est = sm.load(g)
    print('\n')
    print(est.predict(df1))
    
    
    
print("Enter GPA ")
c=(float)(input())
b=(float)(input())
a=(float)(input())
print("\nSelect Your Branch from the following options\n")
print ("'CE', 'MC', 'EC', 'AE', 'SE', 'BT', 'PE', 'PS', 'EN', 'EP', 'EL', 'ME', 'CO', 'IT', 'EE'")
branch=input()
predictor(a,b,c,branch)