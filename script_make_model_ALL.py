def make_model1(a):    
    import pandas as pd
    import json
    
    with open('2k15_data_final.json') as json_file:  
        data = json.load(json_file)
    one=[]
    two=[]
    three=[]
    four=[]
    for item in data:
            if (data[item]['branch']==a):
                one.append(data[item]['gpa']['sem2']['sgpa'])
                two.append(data[item]['gpa']['sem3']['sgpa'])
                three.append(data[item]['gpa']['sem4']['sgpa'])    
                four.append(data[item]['gpa']['sem5']['sgpa'])        
    
    '''print(len(one))
    print(len(two))
    print(len(three))
    print(len(four))'''
    
    with open('2k16_data_final.json') as json_file:  
        data = json.load(json_file)
    for item in data:
            if (data[item]['branch']==a):
                one.append(data[item]['gpa']['sem1']['sgpa'])
                two.append(data[item]['gpa']['sem2']['sgpa'])
                three.append(data[item]['gpa']['sem3']['sgpa'])    
                four.append(data[item]['gpa']['sem4']['sgpa'])        
    
    '''print(len(one))
    print(len(two))
    print(len(three))
    print(len(four)'''
    
    d = {'col1': one, 'col2': two,'col3': three, 'col4': four}
    df = pd.DataFrame(data=d)
    #print(len(df))
    
    X=df[['col1','col2','col3']]
    Y=df['col4']
    
    from sklearn.cross_validation import train_test_split
    
    xtrain,xtest,ytrain,ytest=train_test_split(X,Y,test_size=0.1)
    
    #print(xtest)
    
    '''print(len(xtrain))
    print(len(xtest))
    print(len(ytrain))
    print(len(ytest))'''
    
    import statsmodels.api as sm
    
    model = sm.OLS(ytrain,xtrain).fit()
    #print(est.summary())
    
    
    '''d1 = {'col1': [9.29], 'col2': [8.86],'col3': [9.52]}
    df1 = pd.DataFrame(data=d1)
    print(model.predict(df1))'''
    g='PREDICTOR_'+a+'.pickle'
    model.save(g)
    est = sm.load(g)
    print(est.summary())
    from sklearn.metrics import r2_score
    r2=r2_score(ytest,est.predict(xtest))
    print(r2)
   

#plot bar graphhhhhhhh
l = [ 'CE', 'MC', 'EC', 'AE', 'NULL', 'SE', 'BT', 'PE', 'PS', 'EN', 'EP', 'EL', 'ME', 'CO', 'IT', 'EE']
for i in l:
    make_model1(i)